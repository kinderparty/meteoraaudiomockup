State Group	ID	Name			Wwise Object Path	Notes
	3950429679	PowerUp			\Default Work Unit\PowerUp	
	3991942870	Music			\Default Work Unit\Music	

State	ID	Name	State Group			Notes
	0	None	PowerUp			
	930712164	Off	PowerUp			
	1651971902	On	PowerUp			
	0	None	Music			
	84696445	B	Music			
	84696446	A	Music			
	1125500713	Intro	Music			
	4184794294	Outro	Music			

Audio Bus	ID	Name			Wwise Object Path	Notes
	549046773	Fills			\Default Work Unit\Master Audio Bus\Music\Fills	
	1394512590	PowerLayerDrums			\Default Work Unit\Master Audio Bus\Music\PowerLayerDrums	
	3298531235	Layers			\Default Work Unit\Master Audio Bus\Music\Layers	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	
	3991942870	Music			\Default Work Unit\Master Audio Bus\Music	

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

