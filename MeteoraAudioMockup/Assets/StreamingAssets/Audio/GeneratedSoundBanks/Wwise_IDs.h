/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID INTROEND_MUSIC = 1347393532U;
        static const AkUniqueID PLAY_MUSIC = 2932040671U;
        static const AkUniqueID STOP_MUSIC = 2837384057U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace MUSIC
        {
            static const AkUniqueID GROUP = 3991942870U;

            namespace STATE
            {
                static const AkUniqueID A = 84696446U;
                static const AkUniqueID B = 84696445U;
                static const AkUniqueID INTRO = 1125500713U;
                static const AkUniqueID OUTRO = 4184794294U;
            } // namespace STATE
        } // namespace MUSIC

        namespace POWERUP
        {
            static const AkUniqueID GROUP = 3950429679U;

            namespace STATE
            {
                static const AkUniqueID OFF = 930712164U;
                static const AkUniqueID ON = 1651971902U;
            } // namespace STATE
        } // namespace POWERUP

    } // namespace STATES

    namespace SWITCHES
    {
        namespace MUSIC_STATE
        {
            static const AkUniqueID GROUP = 3826569560U;

            namespace SWITCH
            {
                static const AkUniqueID A = 84696446U;
                static const AkUniqueID B = 84696445U;
                static const AkUniqueID OUTRO = 4184794294U;
            } // namespace SWITCH
        } // namespace MUSIC_STATE

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MUSIC = 3991942870U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID FILLS = 549046773U;
        static const AkUniqueID LAYERS = 3298531235U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID POWERLAYERDRUMS = 1394512590U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
