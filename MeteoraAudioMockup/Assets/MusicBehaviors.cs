﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBehaviors : MonoBehaviour
{
    public AK.Wwise.Bank MusicBank = null;
    public AK.Wwise.Event PlayEvent = null;
    public AK.Wwise.Event StopEvent = null;
    public AK.Wwise.State MusicState_Intro;
    public AK.Wwise.State MusicState_A;
    public AK.Wwise.State MusicState_B;
    public AK.Wwise.State MusicState_Outro;

    public void Awake()
    {
        MusicBank.Load();
        MusicState_Intro.SetValue();
    }

    public void PlayMusic()
    {
        MusicState_Intro.SetValue();
        PlayEvent.Post(gameObject);
       
    }


    public void StopMusic()
    {
        MusicState_Outro.SetValue();
    }

    public void SetMusicState_A()
    {
        MusicState_A.SetValue();
    }

    public void SetMusicState_B()
    {
        MusicState_B.SetValue();
    }


}
